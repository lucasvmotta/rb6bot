#!/usr/bin/env python
# -*- coding: utf-8 -*-
import scrapy
import json


#Dicionario com os elos
eloList = [
	{'value':0,'string':'Copper 4',},
	{'value':1400,'string':'Copper 3',},
	{'value':1500,'string':'Copper 2',},
	{'value':1600,'string':'Copper 1',},
	{'value':1700,'string':'Bronze 4',},
	{'value':1800,'string':'Bronze 3',},
	{'value':1900,'string':'Bronze 2',},
	{'value':2000,'string':'Bronze 1',},
	{'value':2100,'string':'Silver 4',},
	{'value':2200,'string':'Silver 3',},
	{'value':2300,'string':'Silver 2',},
	{'value':2400,'string':'Silver 1',},
	{'value':2500,'string':'Gold 4',},
	{'value':2700,'string':'Gold 3',},
	{'value':2900,'string':'Gold 2',},
	{'value':3100,'string':'Gold 1',},
	{'value':3300,'string':'Platinum 3',},
	{'value':3700,'string':'Platinum 2',},
	{'value':4100,'string':'Platinum 1',},
	{'value':4500,'string':'Diamond'}]

#Instancia do robo
class Rb6Spider(scrapy.Spider):
	name = "rb6"

	def __init__(self, username=''):
		self.username = username

	#Request inicial, executa um request na pagina principal do site, e manda o retorno pra funcao send_user
	def start_requests(self):
		print('start_requests')
		yield scrapy.Request(url='https://r6db.com/',callback=self.send_user)

	#Abre a pagina inicial, e efetua uma pesquisa passando o nome de usuario (pelo que vi no site, apenas pesquisar pela url nao funciona, precisa fazer uma chamada na api em seguida)
	#x-app-id foi um campo que encontrei verificando o request, eh um campo estatico que eh necessario pra fazer os requests posteriores
	def send_user(self,response):
		print('send_user')
		return scrapy.Request(url='https://r6db.com/search/PC/%s' % self.username, callback=self.call_users_api,headers={'x-app-id': 'b0815d12-ce26-462f-85ec-b866f24db0f0'})

	#Efetua uma chamada na api (basicamente replica o request acima, mas chamando em outra url, igual o navegador faz)
	def call_users_api(self,response):
		print('call_users_api')
		return scrapy.Request(url='https://r6db.com/api/v2/players?name=%s&platform=PC' % self.username, callback=self.parse_users_list,headers={'x-app-id': 'b0815d12-ce26-462f-85ec-b866f24db0f0'})

	#Retorno da api de usuarios, retorna um json com todos os usuarios da pesquisa, eu utilizo o campo userId para acessar o perfil do usuario posteriormente
	#Verifico se o nome do usuario eh o mesmo nome que eu procuro, se for, faco uma chamada na api, a chamada na api vai precisar do userId (pra replicar a chamada atual), por isso , envio o userId no request.meta
	def parse_users_list(self, response):
		print('parse_json')
		for item in json.loads(response.body_as_unicode()):
			if item.get('name') == self.username:
				return scrapy.Request(url='https://r6db.com/player/%s' % item.get('userId'), callback=self.call_api_player,meta={'userId':item.get('userId')})
		print 'Usuario %s NAO ENCONTRADO' % self.username

	#Executo a call na api de usuarios, utilizando o userId passado anteriormente
	#Manda um json com os dados do perfil pra funcao parse_profile
	def call_api_player(self,response):		
		print('call_api_player')
		return scrapy.Request(url='https://r6db.com/api/v2/players/%s?platform=PC&update=false' % response.meta['userId'], callback=self.parse_profile,headers={'x-app-id': 'b0815d12-ce26-462f-85ec-b866f24db0f0'})

	#Essa funcao retorna um json com todos dados que voce ve na tela de perfil do usuario
	#Print( o nome do usuario, o mmr south america (ncsa), e o elo (se houver))
	def parse_profile(self,response):
		body_as_json = json.loads(response.body_as_unicode())
		try:
			#Compar o mmr com o dicionario, caso o mmr seja menor que 1400, ele considera o player copper 4
			for index,eloDict in enumerate(eloList):
				if float(body_as_json.get('rank').get('ncsa').get('mmr')) <= eloList[1].get('value'):
					elo = eloList[0]
					break
				#Caso o mmr seja menor que o proximo nivel, ele considera o usuario nivel atual
				if float(body_as_json.get('rank').get('ncsa').get('mmr')) < eloList[index+1].get('value'):
					elo = eloList[index]
					break
		except:
			#Caso o mmr seja diamond, ele vai dar erro por tentar acessar o proximo valor da lista, e caira aqui
			elo = eloList[-1]
		finally:
			#Resultado final
			print('User: %s' % self.username)
			print('MMR South America: %s' % float(body_as_json.get('rank').get('ncsa').get('mmr')))
			print('ELO South America: %s' % elo.get('string'))
