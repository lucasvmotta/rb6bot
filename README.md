# rb6bot

Simple crawler  to get user data from r6db.com (mmr and rank on south america)

installation:
pip install -r requirements.txt

usage:
scrapy crawl rb6 -a username=username

example:
scrapy crawl rb6 -a username=Paulo.2k

User: Paulo.2k
MMR South America: 2370.05544892
ELO South America: Silver 2

